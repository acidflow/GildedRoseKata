package com.gildedrose

import org.scalatest._

class GildedRoseTest extends FlatSpec with Matchers {
  behavior of "Default item"

  it should "decrease both values for a normal item" in {
    val items = Array[Item](new Item("foo", 0, 1))
    val app = new GildedRose(items)
    app.updateQuality()

    app.items(0).quality should equal(0)
    app.items(0).sellIn should equal(-1)
  }

  it should "never have a negative quality" in {
    val items = Array[Item](new Item("foo", 10, 0))
    val app = new GildedRose(items)
    app.updateQuality()

    app.items(0).quality should equal(0)
    app.items(0).sellIn should equal(9)
  }

  it should "decrease the quality twice as fast after sell in" in {
    val items = Array[Item](new Item("foo", 0, 10))
    val app = new GildedRose(items)
    app.updateQuality()

    app.items(0).quality should equal(8)
    app.items(0).sellIn should equal(-1)
  }


  behavior of "Aged Brie"

  it should "increase its value the older it gets" in {
    val items = Array[Item](new Item("Aged Brie", 1, 0))
    val app = new GildedRose(items)
    app.updateQuality()

    app.items(0).quality should equal(1)
    app.items(0).sellIn should equal(0)
  }

  it should "increase its value twice as fast after sell in" in {
    val items = Array[Item](new Item("Aged Brie", 0, 0))
    val app = new GildedRose(items)
    app.updateQuality()

    app.items(0).quality should equal(2)
    app.items(0).sellIn should equal(-1)
  }

  it should "never exceed quality 50" in {
    val items = Array[Item](new Item("Aged Brie", 1, 50))
    val app = new GildedRose(items)
    app.updateQuality()

    app.items(0).quality should equal(50)
    app.items(0).sellIn should equal(0)
  }

  behavior of "Sulfuras, Hand of Ragnaros"

  it should "never be sold and never decrease in quality" in {
    val items = Array[Item](new Item("Sulfuras, Hand of Ragnaros", 1, 80))
    val app = new GildedRose(items)
    app.updateQuality()

    app.items(0).quality should equal(80)
    app.items(0).sellIn should equal(1)
  }

  behavior of "Backstage passes"

  it should "increase quality every day by one until 10 days left" in {
    val items = Array[Item](new Item("Backstage passes to a TAFKAL80ETC concert", 20, 10))
    val app = new GildedRose(items)
    app.updateQuality()

    app.items(0).quality should equal(11)
    app.items(0).sellIn should equal(19)
  }

  it should "increase quality by 2 between 10 & 5 days left" in {
    val items = Array[Item](new Item("Backstage passes to a TAFKAL80ETC concert", 10, 10))
    val app = new GildedRose(items)
    for(i <- 1 to 5) {
      app.updateQuality()

      app.items(0).quality should equal(10 + 2 * i)
      app.items(0).sellIn should equal(10 - i)
    }

  }

  it should "increase quality by 3 between 5 & 1 days left" in {
    val items = Array[Item](new Item("Backstage passes to a TAFKAL80ETC concert", 5, 10))
    val app = new GildedRose(items)
    for(i <- 1 to 5) {
      app.updateQuality()

      app.items(0).quality should equal(10 + 3 * i)
      app.items(0).sellIn should equal(5 - i)
    }

  }

  it should "have a quality of 0 after sell in" in {
    val items = Array[Item](new Item("Backstage passes to a TAFKAL80ETC concert", 0, 10))
    val app = new GildedRose(items)
    app.updateQuality()

    app.items(0).quality should equal(0)
    app.items(0).sellIn should equal(-1)
  }

  it should "never exceed a quality of 50" in {
    val items = Array[Item](new Item("Backstage passes to a TAFKAL80ETC concert", 5, 49))
    val app = new GildedRose(items)
      app.updateQuality()

    app.items(0).quality should equal(50)
    app.items(0).sellIn should equal(4)
  }


  behavior of "Conjured items"

  it should "never have a negative quality" in {
    val items = Array[Item](new Item("Conjured Mana Cake", 0, 1))
    val app = new GildedRose(items)
    app.updateQuality()

    app.items(0).quality should equal(0)
    app.items(0).sellIn should equal(-1)
  }

  it should "decrease twice as fast as a normal item" in {
    val items = Array[Item](new Item("Conjured Mana Cake", 1, 2))
    val app = new GildedRose(items)
    app.updateQuality()

    app.items(0).quality should equal(0)
    app.items(0).sellIn should equal(0)
  }

  it should "decrease the quality twice as fast after sell in" in {
    val items = Array[Item](new Item("Conjured Mana Cake", 0, 10))
    val app = new GildedRose(items)
    app.updateQuality()

    app.items(0).quality should equal(6)
    app.items(0).sellIn should equal(-1)
  }
}