package com.gildedrose

case class UpdaterService(sellInUpdater: SellInUpdater, qualityUpdater: QualityUpdater) {

  def update(item: Item): Unit = {
    qualityUpdater.update(item)
    sellInUpdater.update(item)
  }

}
