package com.gildedrose

sealed trait SellInUpdater {
  def update(item: Item): Unit
}

case class DecreasingSellIn() extends SellInUpdater {
  def update(item: Item): Unit = {
    item.sellIn -= 1
  }
}

case class NoOpSellIn() extends SellInUpdater {
  def update(item: Item): Unit = {
    item.sellIn = item.sellIn
  }
}