package com.gildedrose

import com.gildedrose.QualityUpdater._

object UpdaterServiceFactory {

  def apply(item: Item): UpdaterService = {

    implicit def asUpperBoundedUpdater(p: (Int, QualityUpdater)): UpperBoundedUpdater = UpperBoundedUpdater(p._1, p._2)

    item.name match {
      case "Aged Brie" => UpdaterService(
        DecreasingSellIn(),
        ComposedQualityUpdater(
          (0, IncreasingQualityUpdater(DEFAULT_QUALITY_STEP_AFTER_SELLIN)),
          (Int.MaxValue, IncreasingQualityUpdater(DEFAULT_QUALITY_STEP))
        )
      )
      case "Backstage passes to a TAFKAL80ETC concert" =>
        UpdaterService(
          DecreasingSellIn(),
          ComposedQualityUpdater(
            (0, ConstantQualityUpdater(BACKSTAGE_QUALITY_AFTER_CONCERT)),
            (5, IncreasingQualityUpdater(BACKSTAGE_QUALITY_STEP_5_DAYS)),
            (10, IncreasingQualityUpdater(BACKSTAGE_QUALITY_STEP_10_DAYS)),
            (Int.MaxValue, IncreasingQualityUpdater(DEFAULT_QUALITY_STEP))
          )
        )
      case "Sulfuras, Hand of Ragnaros" => UpdaterService(
        NoOpSellIn(), NoOpQualityUpdater()
      )
      case "Conjured Mana Cake" =>
        UpdaterService(
          DecreasingSellIn(), ComposedQualityUpdater(
            (0, DecreasingQualityUpdater(CONJURED_QUALITY_STEP_AFTER_SELLIN)),
            (Int.MaxValue, DecreasingQualityUpdater(CONJURED_QUALITY_STEP))
          )
        )
      case _ =>
        UpdaterService(
          DecreasingSellIn(), ComposedQualityUpdater(
            (0, DecreasingQualityUpdater(DEFAULT_QUALITY_STEP_AFTER_SELLIN)),
            (Int.MaxValue, DecreasingQualityUpdater(DEFAULT_QUALITY_STEP))
          )
        )
    }
  }

}
