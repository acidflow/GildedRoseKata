package com.gildedrose

class GildedRose(val items: Array[Item]) {

  def updateQuality(): Unit = items.foreach(item => UpdaterServiceFactory(item).update(item))

}