package com.gildedrose

import scala.math._

object QualityUpdater {
  val MIN_QUALITY = 0
  val MAX_QUALITY = 50

  val DEFAULT_QUALITY_STEP = 1
  val DEFAULT_QUALITY_STEP_AFTER_SELLIN = 2 * DEFAULT_QUALITY_STEP

  val CONJURED_QUALITY_STEP = 2 * DEFAULT_QUALITY_STEP
  val CONJURED_QUALITY_STEP_AFTER_SELLIN = 2 * DEFAULT_QUALITY_STEP_AFTER_SELLIN

  val BACKSTAGE_QUALITY_STEP_10_DAYS = 2
  val BACKSTAGE_QUALITY_STEP_5_DAYS = 3
  val BACKSTAGE_QUALITY_AFTER_CONCERT = 0
}

sealed trait QualityUpdater {
  def update(item: Item): Unit
}


case class NoOpQualityUpdater() extends QualityUpdater {
  override def update(item: Item): Unit = {
    item.quality = item.quality
  }
}

case class ConstantQualityUpdater(value: Int) extends QualityUpdater {
  override def update(item: Item): Unit = {
    item.quality = value
  }
}

case class DecreasingQualityUpdater(step: Int) extends QualityUpdater {
  override def update(item: Item): Unit = {
    item.quality = max(item.quality - step, QualityUpdater.MIN_QUALITY)
  }
}

case class IncreasingQualityUpdater(step: Int) extends QualityUpdater {
  override def update(item: Item): Unit = {
    item.quality = min(item.quality + step, QualityUpdater.MAX_QUALITY)
  }
}

case class UpperBoundedUpdater(upperBound: Int, qualityUpdater: QualityUpdater)

case class ComposedQualityUpdater(updaters: UpperBoundedUpdater*) extends QualityUpdater {
  val sortedUpdaters = updaters.sortBy(_.upperBound)

  override def update(item: Item): Unit = {
    val updater = sortedUpdaters.find(p => {
      item.sellIn <= p.upperBound
    }).getOrElse(sortedUpdaters.last)

    updater.qualityUpdater.update(item)
  }
}
